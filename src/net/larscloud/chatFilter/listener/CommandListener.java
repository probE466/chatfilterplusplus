package net.larscloud.chatFilter.listener;

import net.larscloud.chatFilter.handler.IO;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;

public class CommandListener implements CommandExecutor {

    @EventHandler
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (cmd.getName().equalsIgnoreCase("index") && sender.hasPermission("chatfilterplusplus.canreload")) {
            IO.getIO().indexFiles();
            sender.sendMessage(ChatColor.GOLD + "Indexing complete!");
            return true;
        }
        if (cmd.getName().equalsIgnoreCase("addphrase") && sender.hasPermission("chatfilterplusplus.canadd")) {
            if (args.length == 0) {
                sender.sendMessage(ChatColor.AQUA + "please add words");
                return true;
            }
            if (args.length == 1) {
                IO.getIO().addPhrase(args[0]);
                return true;
            }
            String returnString = "";
            for (int i = 0; i < args.length; i++) {
                String tmp = args[i];
                returnString += tmp;
                if(i != args.length - 1) {
                    returnString += " ";
                }
            }
            IO.getIO().addPhrase(returnString);

            return true;
        }
        if(cmd.getName().equalsIgnoreCase("addsevereword") && sender.hasPermission("chatfilterplusplus.canadd")) {
            if(args.length == 0) {
                sender.sendMessage(ChatColor.AQUA + "please add words");
                return true;
            }
            if(args.length != 1) {
                sender.sendMessage(ChatColor.RED + "You can only add single words to this file");
                return true;
            }
            IO.getIO().addSevereWord(args[0]);
            return true;
        }
        if(cmd.getName().equalsIgnoreCase("delsevereword") && sender.hasPermission("chatfilterplusplus.candelete")) {
            if(args.length == 0) {
                sender.sendMessage(ChatColor.AQUA + "please add words");
                return true;
            }
            if(args.length != 1) {
                sender.sendMessage(ChatColor.RED + "You can only delete single words to this file");
                return true;
            }
            IO.getIO().removeSevereWord(args[0]);
            return true;
        }
        if (cmd.getName().equalsIgnoreCase("delphrase") && sender.hasPermission("chatfilterplusplus.candelete")) {
            if (args.length == 0) {
                sender.sendMessage(ChatColor.AQUA + "please add words");
                return true;
            }
            if (args.length == 1) {
                IO.getIO().removePhrase(args[0]);
                return true;
            }
            String returnString = "";
            for (int i = 0; i < args.length; i++) {
                String tmp = args[i];
                returnString += tmp;
                if(i != args.length - 1) {
                    returnString += " ";
                }
            }
            IO.getIO().removePhrase(returnString);
            return true;
        }
        return false;
    }
}
