package net.larscloud.chatFilter.listener;

import net.larscloud.chatFilter.ChatFilter;
import net.larscloud.chatFilter.util.Helper;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener{

    @EventHandler
    public void onChat(AsyncPlayerChatEvent chatEvent) {
        if(ChatFilter.listOfForbiddenPhrases.contains(chatEvent.getMessage())
                && !(chatEvent.getPlayer().hasPermission("chatfilterplusplus.isnotpoliced"))) {
            chatEvent.getPlayer().sendMessage(ChatColor.RED + "Phrase is on the forbidden list!");
            chatEvent.setCancelled(true);
            return;
        }
        if(Helper.containsSeverePhrase(chatEvent.getMessage())
                && !(chatEvent.getPlayer().hasPermission("chatfilterplusplus.isnotpoliced"))) {
            chatEvent.getPlayer().sendMessage(ChatColor.RED + "Your message contains banned word(s)");
            chatEvent.setCancelled(true);
        }
    }
}