package net.larscloud.chatFilter;

import net.larscloud.chatFilter.handler.IO;
import net.larscloud.chatFilter.listener.ChatListener;
import net.larscloud.chatFilter.listener.CommandListener;
import org.bukkit.ChatColor;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.ArrayList;

public class ChatFilter extends JavaPlugin {

    public static ArrayList<String> listOfForbiddenPhrases = new ArrayList<>();

    public static ArrayList<String> listOfSevereWords = new ArrayList<>();

    public Permission isPoliced = new Permission("chatfilterplusplus.isnotpoliced");

    public Permission canAdd = new Permission("chatfilterplusplus.canadd");

    public Permission canReload = new Permission("chatfilterplusplus.canreload");

    public Permission canDelete = new Permission("chatfilterplusplus.candelete");

    private PluginManager pluginManager = this.getServer().getPluginManager();

    @Override
    public void onEnable() {
        this.getLogger().info("ChatFilter++ will now index your file...");
        IO.getIO().indexFiles();
        this.getLogger().info("...indexing complete.");
        this.getLogger().info("Thanks for using my Plugin.");
        this.getLogger().info("Starting Chat sniffer");
        chatSniffer();
    }

    private void chatSniffer() {
        pluginManager.addPermission(isPoliced);
        pluginManager.addPermission(canAdd);
        pluginManager.addPermission(canReload);
        pluginManager.addPermission(canDelete);
        this.getCommand("index").setExecutor(new CommandListener());
        this.getCommand("index").setPermissionMessage(ChatColor.RED + "You cannot index the list, you are lacking the " +
                "permissions for it!");
        this.getCommand("addphrase").setExecutor(new CommandListener());
        this.getCommand("addphrase").setPermissionMessage(ChatColor.RED + "You cannot add to the list, you are lacking the " +
                "permissions for it!");
        this.getCommand("delphrase").setExecutor(new CommandListener());
        this.getCommand("delphrase").setPermissionMessage(ChatColor.RED + "You cannot delete from the list, you are lacking the " +
                "permissions for it!");
        this.getCommand("addsevereword").setExecutor(new CommandListener());
        this.getCommand("delsevereword").setExecutor(new CommandListener());
        pluginManager.registerEvents(new ChatListener(), this);
    }


    @Override
    public void onDisable() {
        this.getLogger().info("ChatFilter++ has been disabled, see you soon!");
    }
}
