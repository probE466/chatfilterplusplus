package net.larscloud.chatFilter.util;

import net.larscloud.chatFilter.ChatFilter;

import java.util.ArrayList;
import java.util.Arrays;


public class Helper {
    public static boolean containsSeverePhrase(String message) {
        String result = message.replaceAll("[-+.^:,?!]", "");
        ArrayList<String> messageList = new ArrayList<String>(Arrays.asList(result.split(" ")));
        for (int i = 0; i < messageList.size(); ++i) {
            if (!ChatFilter.listOfSevereWords.contains(messageList.get(i))) continue;
            return true;
        }
        return false;
    }
}

