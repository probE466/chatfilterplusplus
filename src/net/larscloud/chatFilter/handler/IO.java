package net.larscloud.chatFilter.handler;

import net.larscloud.chatFilter.ChatFilter;
import org.bukkit.Bukkit;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;

public class IO {

    private String path = "./plugins/ChatFilter++/bannedPhrases.txt";
    private String severPath = "./plugins/ChatFilter++/severeWords.txt";

    private static IO io = new IO();

    public IO() {
        File file = new File(path);
        File dir = new File("./plugins/ChatFilter++");
        File severeFile = new File(severPath);
        if (!(file.isFile())) {
            try {
                dir.mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: IO COULD NOT BE INITIALIZED. CHECK YOUR PERMISSIONS");
                e.printStackTrace();
            }
        }
        if(!(severeFile.isFile())) {
            try {
                severeFile.createNewFile();
            } catch(IOException ex) {
                Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: IO COULD NOT BE INITIALIZED. CHECK YOUR PERMISSIONS");
                ex.printStackTrace();
            }
        }
    }

    public void addPhrase(String phrase) {
        if(ChatFilter.listOfForbiddenPhrases.contains(phrase)) {
            return;
        }
        try (Writer output = new BufferedWriter(new FileWriter(path, true))) {
            phrase = phrase + "\n";
            output.append(phrase);
            indexFiles();
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: PHRASE COULD NOT BE ADDED, CHECK IF YOU HAVE WRITE PERMISSIONS");
            ex.printStackTrace();
        }
        indexFiles();
    }

    public void addSevereWord(String word) {
        if(ChatFilter.listOfSevereWords.contains(word)) {
            return;
        }
        try( Writer output = new BufferedWriter((new FileWriter(severPath, true)))) {
            word = word + "\n";
            output.append(word);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        indexFiles();
    }

    public void removeSevereWord(String word) {
        if(!(ChatFilter.listOfSevereWords.contains(word.trim()))) {
            return;
        }
        ChatFilter.listOfSevereWords.remove(word.trim());
        try (PrintWriter out = new PrintWriter(severPath)) {
            for (String str : ChatFilter.listOfSevereWords) {
                out.append(str + "\n");
            }
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: PHRASE COULD NOT BE REMOVED, CHECK IF YOU HAVE WRITE PERMISSIONS");
            ex.printStackTrace();
        }
        indexFiles();
    }

    public static IO getIO() {
        return io;
    }

    public void removePhrase(String phrase) {
        if(!(ChatFilter.listOfForbiddenPhrases.contains(phrase.trim()))) {
            return;
        }
        ChatFilter.listOfForbiddenPhrases.remove(phrase.trim());
        try (PrintWriter out = new PrintWriter(path)) {
            for (String str : ChatFilter.listOfForbiddenPhrases) {
                out.append(str + "\n");
            }
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: PHRASE COULD NOT BE REMOVE, CHECK IF YOU HAVE WRITE PERMISSIONS");
            ex.printStackTrace();
        }
        indexFiles();
    }

    public void indexFiles() {
        ChatFilter.listOfSevereWords.clear();
        ChatFilter.listOfForbiddenPhrases.clear();
        try (FileInputStream fstream = new FileInputStream(path); BufferedReader br = new BufferedReader(new InputStreamReader(fstream))) {
            String strLine;
            while ((strLine = br.readLine()) != null) {
                ChatFilter.listOfForbiddenPhrases.add(strLine);
            }
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: File bannedPhrases.txt could not be indexed");
            ex.printStackTrace();
        }
        try (FileInputStream fstream = new FileInputStream(severPath); BufferedReader br = new BufferedReader(new InputStreamReader(fstream))) {
            String strSevereLine;
            while ((strSevereLine = br.readLine()) != null) {
                ChatFilter.listOfSevereWords.add(strSevereLine);
            }
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, "ChatFilter++ :: File severeWords.txt could not be indexed");
            ex.printStackTrace();
        }
    }
}
